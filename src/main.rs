use macroquad::input::KeyCode;
use macroquad::prelude::*;
mod chip8;
use chip8::Chip8;
use std::time::Duration;
use std::env;

const KEYS: [KeyCode; 16] = [
    KeyCode::X,
    KeyCode::Key1,
    KeyCode::Key2,
    KeyCode::Key3,
    KeyCode::Q,
    KeyCode::W,
    KeyCode::E,
    KeyCode::A,
    KeyCode::S,
    KeyCode::D,
    KeyCode::Z,
    KeyCode::C,
    KeyCode::Key4,
    KeyCode::R,
    KeyCode::F,
    KeyCode::V,
];

impl Chip8 {
    fn input(&mut self) {
        for i in 0..16 {
            if is_key_down(KEYS[i]) {
                self.key |= 1 << i;
            } else {
                self.key &= !(1 << i)
            }
        }
    }
}

#[macroquad::main("Chip8 Emulator")]
async fn main() {
    let mut chip = Chip8::new(1200);
    chip.loadfont();

    let args: Vec<String> = env::args().collect();
    if args.len() >= 2 {
        let rom: Vec<u8> = std::fs::read(&args[1]).unwrap();
        chip.loadrom(&rom);
    }

    loop {
        let width_scale = screen_width() / chip8::CHIP_WIDTH as f32;
        let height_scale = screen_height() / chip8::CHIP_HEIGHT as f32;
        let nops = chip.clock_speed / 60;

        chip.input();
        chip.update_timer();
        for _ in 0..nops as u32 {
            chip.interpret();
        }

        clear_background(BLACK);

        for (y, row) in chip.fb.iter().enumerate() {
            for (x, &col) in row.iter().enumerate() {
                draw_rectangle(
                    x as f32 * width_scale,
                    y as f32 * height_scale,
                    width_scale,
                    height_scale,
                    if col { GREEN } else { BLACK },
                );
            }
        }
 
        std::thread::sleep(Duration::from_millis(15));
        next_frame().await
    }
}
