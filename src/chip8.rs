use rand::Rng;

const MEM_SIZE: usize = 30000;
const REG_COUNT: usize = 16;
pub const CHIP_WIDTH: usize = 64;
pub const CHIP_HEIGHT: usize = 32;

const FONT: [u8; 80] = [
    0xF0, 0x90, 0x90, 0x90, 0xF0,
    0x20, 0x60, 0x20, 0x20, 0x70,
    0xF0, 0x10, 0xF0, 0x80, 0xF0,
    0xF0, 0x10, 0xF0, 0x10, 0xF0,
    0x90, 0x90, 0xF0, 0x10, 0x10,
    0xF0, 0x80, 0xF0, 0x10, 0xF0,
    0xF0, 0x80, 0xF0, 0x90, 0xF0,
    0xF0, 0x10, 0x20, 0x40, 0x40,
    0xF0, 0x90, 0xF0, 0x90, 0xF0,
    0xF0, 0x90, 0xF0, 0x10, 0xF0,
    0xF0, 0x90, 0xF0, 0x90, 0x90,
    0xE0, 0x90, 0xE0, 0x90, 0xE0,
    0xF0, 0x80, 0x80, 0x80, 0xF0,
    0xE0, 0x90, 0x90, 0x90, 0xE0,
    0xF0, 0x80, 0xF0, 0x80, 0xF0,
    0xF0, 0x80, 0xF0, 0x80, 0x80
];



pub struct Chip8 {
    pc: usize,
    i: usize,
    v: Vec<u8>,
    memory: Vec<u8>,
    pub key: u32,
    pub fb: Vec<Vec<bool>>,
    stack: Vec<usize>,
    pub clock_speed: u32,
    delay_timer: u8,
    sound_timer: u8,
    paused: bool,
    rng: rand::rngs::ThreadRng,
}

impl Chip8 {
    pub fn new(clock_speed: u32) -> Self {
        Self {
            pc: 0x200,
            i: 0,
            v: vec![0; REG_COUNT],
            fb: vec![vec![false; CHIP_WIDTH]; CHIP_HEIGHT],
            stack: vec![],
            memory: vec![0; MEM_SIZE],
            clock_speed,
            delay_timer: 0,
            sound_timer: 0,
            key: 0,
            paused: false,
            rng: rand::thread_rng(),
        }
    }

    pub fn loadfont(&mut self) {
        for i in 0..80 {
            self.memory[i] = FONT[i];
        }
    }

    pub fn loadrom(&mut self, rom: &Vec<u8>) {
        for (i, &byte) in rom.iter().enumerate() {
            self.memory[0x200+i] = byte;
        }
    }

    pub fn interpret(&mut self) {
        let op: u16 = ((self.memory[self.pc] as u16) << 8) |
            self.memory[self.pc+1] as u16;
        let x: usize = ((op & 0x0F00) >> 8) as usize;
        let y: usize = ((op & 0x00F0) >> 4) as usize;
        let nnn: u16 = op & 0x0FFF;
        let nn: u8 = op as u8;
        let n: u8 = op as u8 & 0xF;

        self.pc += 2;
        match op & 0xF000 {
            0x0000 => match nn {
                0xE0 => self.op_00e0(),
                0xEE => self.op_00ee(),
                _ => {},
            },
            0x1000 => self.op_1nnn(nnn),
            0x2000 => self.op_2nnn(nnn),
            0x3000 => self.op_3xnn(x, nn),
            0x4000 => self.op_4xnn(x, nn),
            0x5000 => self.op_5xy0(x, y),
            0x6000 => self.op_6xnn(x, nn),
            0x7000 => self.op_7xnn(x, nn),
            0x8000 => match n {
                0 => self.op_8xy0(x, y),
                1 => self.op_8xy1(x, y),
                2 => self.op_8xy2(x, y),
                3 => self.op_8xy3(x, y),
                4 => self.op_8xy4(x, y),
                5 => self.op_8xy5(x, y),
                6 => self.op_8xy6(x, y),
                7 => self.op_8xy7(x, y),
                0xE => self.op_8xye(x, y),
                _ => {},
            },
            0x9000 => self.op_9xy0(x, y),
            0xA000 => self.op_annn(nnn),
            0xB000 => self.op_bnnn(nnn),
            0xC000 => self.op_cxnn(x, nn),
            0xD000 => self.op_dxyn(x, y, n),
            0xE000 => match nn {
                0x9E => self.op_ex9e(x),
                0xA1 => self.op_exa1(x),
                _ => {},
            }
            0xF000 => match nn {
                0x07 => self.op_fx07(x),
                0x0A => self.op_fx0a(x),
                0x15 => self.op_fx15(x),
                0x18 => self.op_fx18(x),
                0x1e => self.op_fx1e(x),
                0x29 => self.op_fx29(x),
                0x33 => self.op_fx33(x),
                0x55 => self.op_fx55(x),
                0x65 => self.op_fx65(x),
                _ => {},
            }
            _ => {},
        }
    }

    pub fn update_timer(&mut self) {
        if self.delay_timer > 0 {
            self.delay_timer -= 1;
        }
        if self.sound_timer > 0 {
            self.sound_timer -= 1;
        }
    }

    fn op_00e0(&mut self) {
        self.fb
            .iter_mut()
            .for_each(|r| r.iter_mut().for_each(|c| *c = false));
    }

    fn op_00ee(&mut self) {
        self.pc = self.stack.pop().unwrap();
    }

    fn op_1nnn(&mut self, addr: u16) {
        self.pc = addr as usize;
    }

    fn op_2nnn(&mut self, addr: u16) {
        /* NOTE - this should be self.pc - 2
         * theoretically, but it doesn't work
         * self.pc on the other hand works. Not sure why
         * 💀 */
        self.stack.push(self.pc - 0);
        self.pc = addr as usize;
    }

    fn op_3xnn(&mut self, x: usize, nn: u8) {
        if self.v[x] == nn {
            self.pc += 2;
        }
    }

    fn op_4xnn(&mut self, x: usize, nn: u8) {
        if self.v[x] != nn {
            self.pc += 2;
        }
    }

    fn op_5xy0(&mut self, x: usize, y: usize) {
        if self.v[x] == self.v[y] {
            self.pc += 2;
        }
    }

    fn op_6xnn(&mut self, x: usize, nn: u8) {
        self.v[x] = nn;
    }

    fn op_7xnn(&mut self, x: usize, nn: u8) {
        self.v[x] = self.v[x].wrapping_add(nn);
    }

    fn op_8xy0(&mut self, x: usize, y: usize) {
        self.v[x] = self.v[y];
    }

    fn op_8xy1(&mut self, x: usize, y: usize) {
        self.v[x] |= self.v[y];
    }

    fn op_8xy2(&mut self, x: usize, y: usize) {
        self.v[x] &= self.v[y];
    }

    fn op_8xy3(&mut self, x: usize, y: usize) {
        self.v[x] ^= self.v[y];
    }

    fn op_8xy4(&mut self, x: usize, y: usize) {
        self.v[0xF] = if self.v[x] as u16 + self.v[y] as u16 > 255 { 1 } else { 0 };
        self.v[x] = self.v[x].wrapping_add(self.v[y]);
    }

    fn op_8xy5(&mut self, x: usize, y: usize) {
        self.v[0xF] = if self.v[x] > self.v[y] { 1 } else { 0 };
        self.v[x] = self.v[x].wrapping_sub(self.v[y]);
    }

    fn op_8xy6(&mut self, x: usize, _y: usize) {
        self.v[0xF] = self.v[x] & 1;
        self.v[x] >>= 1;
    }

    fn op_8xy7(&mut self, x: usize, y: usize) {
        self.v[0xF] = if self.v[y] > self.v[x] { 1 } else { 0 };
        self.v[x] = self.v[y].wrapping_sub(self.v[x]);
    }

    fn op_8xye(&mut self, x: usize, _y: usize) {
        self.v[0xF] = (self.v[x] & 0b10000000) >> 7;
        self.v[x] = self.v[x].wrapping_mul(2);
    }

    fn op_9xy0(&mut self, x: usize, y: usize) {
        if self.v[x] != self.v[y] {
            self.pc += 2;
        }
    }

    fn op_annn(&mut self, addr: u16) {
        self.i = addr as usize;
    }

    fn op_bnnn(&mut self, addr: u16) {
        self.pc = addr as usize + self.v[0] as usize;
    }

    fn op_cxnn(&mut self, x: usize, nn: u8) {
        self.v[x] = nn & self.rng.gen::<u8>();
    }

    fn op_dxyn(&mut self, x: usize, y: usize, n: u8) {
        self.v[0xF] = 0;
        for row in 0..n {
            let sprite: u8 = self.memory[self.i + row as usize];
            let yi: usize = (self.v[y] + row) as usize % CHIP_HEIGHT;
            for col in 0..8 {
                if sprite >> (7 - col) & 1 == 0 {
                    continue;
                }
                let xi: usize = (self.v[x] + col) as usize % CHIP_WIDTH;
                if self.fb[yi][xi] {
                    self.v[0xF] = 1;
                }
                self.fb[yi][xi] ^= true;
            }
        }
    }

    fn op_ex9e(&mut self, x: usize) {
        if self.key & (1 << self.v[x]) != 0 {
            self.pc += 2;
        }
    }

    fn op_exa1(&mut self, x: usize) {
        if self.key & (1 << self.v[x]) == 0 {
            self.pc += 2;
        }
    }

    fn op_fx07(&mut self, x: usize) {
        self.v[x] = self.delay_timer;
    }

    fn op_fx0a(&mut self, x: usize) {
        self.pc -= 2;
        for i in 0..16 {
            if self.key >> i & 1 == 1{
                self.v[x] = i;
                self.pc += 2;
                break;
            }
        }
    }

    fn op_fx15(&mut self, x: usize) {
        self.delay_timer = self.v[x];
    }

    fn op_fx18(&mut self, x: usize) {
        self.sound_timer = self.v[x];
    }

    fn op_fx1e(&mut self, x: usize) {
        self.i += self.v[x] as usize;
        self.v[0xF] = if self.i > 0x0FFF { 1 } else { 0 };
    }

    fn op_fx29(&mut self, x: usize) {
        self.i = self.v[x] as usize * 5;
    }

    fn op_fx33(&mut self, x: usize) {
        self.memory[self.i] = self.v[x] / 100;
        self.memory[self.i + 1] = (self.v[x] % 100) / 10;
        self.memory[self.i + 2] = self.v[x] % 10;
    }

    fn op_fx55(&mut self, x: usize) {
        for i in 0..=x {
            self.memory[self.i + i] = self.v[i];
        }
    }

    fn op_fx65(&mut self, x: usize) {
        for i in 0..=x {
            self.v[i] = self.memory[self.i + i];
        }
    }

}
