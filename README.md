# Chip8 Emulator in rust!!

### installation

    git clone https://codeberg.org/sleepntsheep/sheep8.rs
    cd sheep8.rs
    cargo install --path .

### TODO
    
    - arg parsing for speed, etc.
    - maybe config file or arg for custom color
    - nice debug ui
    - envlogger

